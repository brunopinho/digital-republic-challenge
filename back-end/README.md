# Rotas
## `/v1/paints`

Estrutura da requisição: 
```
{
	"walls": [
		{
			"height": 3.00, 
			"width": 2.20,
			"doors": 1,
			"windows": 0
		},
		{
			"height": 4.00, 
			"width": 3.50,
			"doors": 0,
			"windows": 0
		},
		{
			"height": 5.00, 
			"width": 7.20,
			"doors": 0,
			"windows": 0
		},
		{
			"height": 3.50, 
			"width": 2.40,
			"doors": 0,
			"windows": 0
		}
	]
}
```

Estrutura do retorno:
```
{
	"data": {
		"18": 0,
		"3.6": 3,
		"2.5": 0,
		"0.5": 3
	}
}
```
